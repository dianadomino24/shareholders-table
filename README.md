# ShareholdersTable

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.4.

## How to launch

1. Run `npm run mock:server`
2. Open new terminal and run `npm run start:proxy`
3. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you want to change some data in the table, go to './mocks/data.json' file.
