import { Component, Inject, OnInit } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Shareholder } from '../app.component';

@Component({
  selector: 'app-edit-shareholder',
  templateUrl: './edit-shareholder.component.html',
  styleUrls: ['./edit-shareholder.component.scss'],
})
export class EditShareholderComponent implements OnInit {
  name: string;
  quantity: number;
  type: string;

  constructor(
    public dialogRef: MatDialogRef<EditShareholderComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      id: number;
      shareholder: Shareholder;
      editApi: (id: number, shareholder: Shareholder) => void;
    }
  ) {
    this.name = data.shareholder.name;
    this.quantity = data.shareholder.quantity;
    this.type = data.shareholder.type;
  }
  ngOnInit(): void {}

  close() {
    this.dialogRef.close();
  }

  onFormSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    form.value.quantity = Number(form.value.quantity);
    this.data.editApi(this.data.id, form.value);
    this.close();
  }
}
