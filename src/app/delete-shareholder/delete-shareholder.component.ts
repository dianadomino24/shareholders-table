import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-shareholder',
  templateUrl: './delete-shareholder.component.html',
  styleUrls: ['./delete-shareholder.component.scss'],
})
export class DeleteShareholderComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteShareholderComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { id: number; deleteApi: (id: number) => void }
  ) {}

  ngOnInit(): void {}
  close() {
    this.dialogRef.close();
  }

  onFormSubmit(form: NgForm) {
    this.data.deleteApi(this.data.id);
    this.close();
  }
}
