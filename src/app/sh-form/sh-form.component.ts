import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Shareholder } from '../app.component';

@Component({
  selector: 'app-sh-form',
  templateUrl: './sh-form.component.html',
  styleUrls: ['./sh-form.component.scss'],
})
export class ShFormComponent implements OnInit {
  @Input() addApi!: (shareholder: Shareholder) => void;
  @Input() totalErrorText!: string;

  name: string;
  quantity: number;

  showValidationErrors = false;

  constructor() {
    this.name = '';
    this.quantity = 0;
  }

  ngOnInit(): void {}

  onFormSubmit(form: NgForm) {
    if (form.invalid) {
      this.showValidationErrors = true;
      return;
    }
    form.value.quantity = Number(form.value.quantity);
    this.addApi(form.value);
    this.showValidationErrors = false;
    form.reset();
  }
}
