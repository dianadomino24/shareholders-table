import { Component, Input, OnInit } from '@angular/core';
import { CrudService } from './shared/crud.service';

export interface Shareholder {
  id: number;
  name: string;
  quantity: number;
  type: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'shareholders-table';
  totalErrorText = '';
  shareholders: Shareholder[] = [];
  sum = 0;

  constructor(public crudService: CrudService) {}

  ngOnInit() {
    this.fetchShareholders();
  }

  addApi = (shareholder: Shareholder): void => {
    const existing = this.shareholders.find(
      (sh: Shareholder) =>
        sh.name === shareholder.name && sh.type === shareholder.type
    );
    if (!existing) {
      this.crudService.addShareholder(shareholder).subscribe((res) => {
        this.fetchShareholders();
        this.totalErrorText = '';
      });
    } else {
      this.totalErrorText = 'This user already has shares of this type';
      throw Error('This user already has shares of this type');
    }
  };

  editApi = (id: number, shareholder: Shareholder): void => {
    this.crudService.updateShareholder(id, shareholder).subscribe((res) => {
      this.fetchShareholders();
    });
  };
  deleteApi = (id: number): void => {
    this.crudService.deleteShareholder(id).subscribe((res) => {
      this.fetchShareholders();
    });
  };
  makeSplit = (ratio: number): void => {
    const updatedShareholders = this.shareholders.map((sh: Shareholder) => ({
      ...sh,
      quantity: sh.quantity * ratio,
    }));
    updatedShareholders.forEach((sh: Shareholder) => {
      this.crudService.updateShareholder(sh.id, sh).subscribe((res) => {
        const foundIndex = this.shareholders.findIndex((x) => x.id === sh.id);
        this.shareholders[foundIndex] = sh;
      });
    });
    this.sum = this.sum * ratio;
  };

  updateSum() {
    const sum = this.shareholders.reduce(
      (accum, sh: Shareholder) => accum + sh.quantity,
      0
    );
    this.sum = sum;
  }

  fetchShareholders() {
    return this.crudService
      .getShareholders()
      .subscribe((res: Shareholder[]) => {
        this.shareholders = res;
        this.updateSum();
      });
  }
}
