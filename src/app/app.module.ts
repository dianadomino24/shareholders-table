import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SplitFormComponent } from './split-form/split-form.component';
import { ShFormComponent } from './sh-form/sh-form.component';
import { ShTableComponent } from './sh-table/sh-table.component';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { EditShareholderComponent } from './edit-shareholder/edit-shareholder.component';
import { DeleteShareholderComponent } from './delete-shareholder/delete-shareholder.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';

@NgModule({
  declarations: [
    AppComponent,
    SplitFormComponent,
    ShFormComponent,
    ShTableComponent,
    EditShareholderComponent,
    DeleteShareholderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule,
    TrimValueAccessorModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
