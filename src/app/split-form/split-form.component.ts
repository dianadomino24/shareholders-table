import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-split-form',
  templateUrl: './split-form.component.html',
  styleUrls: ['./split-form.component.scss'],
})
export class SplitFormComponent implements OnInit {
  @Input() sum!: number;
  @Input() makeSplit!: (ratio: number) => void;

  constructor() {}

  ngOnInit(): void {}

  onFormSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    form.value.ratio = Number(form.value.ratio);
    this.makeSplit(form.value.ratio);
  }
}
