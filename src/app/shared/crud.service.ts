import { Injectable } from '@angular/core';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Shareholder } from '../app.component';

@Injectable({
  providedIn: 'root',
})
export class CrudService {
  endpoint = 'http://localhost:3000';

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  getShareholders(): Observable<Shareholder[]> {
    return this.httpClient
      .get<Shareholder[]>(this.endpoint + '/shareholders')
      .pipe(retry(1), catchError(this.processError));
  }

  // getSingleShareholder(id: number): Observable<Shareholder> {
  //   return this.httpClient
  //     .get<Shareholder>(this.endpoint + '/shareholders/' + id)
  //     .pipe(retry(1), catchError(this.processError));
  // }

  addShareholder(shareholder: Shareholder): Observable<Shareholder> {
    return this.httpClient
      .post<Shareholder>(
        this.endpoint + '/shareholders',
        JSON.stringify(shareholder),
        this.httpHeader
      )
      .pipe(retry(1), catchError(this.processError));
  }

  updateShareholder(
    id: number,
    shareholder: Shareholder
  ): Observable<Shareholder> {
    return this.httpClient
      .put<Shareholder>(
        this.endpoint + '/shareholders/' + id,
        JSON.stringify(shareholder),
        this.httpHeader
      )
      .pipe(retry(1), catchError(this.processError));
  }

  deleteShareholder(id: number) {
    return this.httpClient
      .delete<Shareholder>(
        this.endpoint + '/shareholders/' + id,
        this.httpHeader
      )
      .pipe(retry(1), catchError(this.processError));
  }

  processError(err: any) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
  }
}
