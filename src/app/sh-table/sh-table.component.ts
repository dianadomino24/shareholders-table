import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteShareholderComponent } from '../delete-shareholder/delete-shareholder.component';
import { EditShareholderComponent } from '../edit-shareholder/edit-shareholder.component';
import { CrudService } from '../shared/crud.service';
import { Shareholder } from '../app.component';

@Component({
  selector: 'app-sh-table',
  templateUrl: './sh-table.component.html',
  styleUrls: ['./sh-table.component.scss'],
})
export class ShTableComponent implements OnInit {
  @Input() shareholders!: Shareholder[];
  @Input() sum!: number;
  @Input() deleteApi!: (id: number) => void;
  @Input() editApi!: (id: number, shareholder: Shareholder) => void;

  constructor(public crudService: CrudService, private dialog: MatDialog) {}

  ngOnInit(): void {}

  editShareholder(shId: number, currentShareholder: Shareholder) {
    const dialogRef = this.dialog.open(EditShareholderComponent, {
      width: '450px',
      data: {
        id: shId,
        shareholder: currentShareholder,
        editApi: this.editApi,
      },
    });
  }

  deleteShareholder(shId: number) {
    const dialogRef = this.dialog.open(DeleteShareholderComponent, {
      width: '384px',
      data: {
        id: shId,
        deleteApi: this.deleteApi,
      },
    });
  }
}
